{
  "name": "RPG Item Generator",
  "description": "Generates an RPG item given an enemy and its difficulty",
  "context": "[ BODY PARTS GENERATOR: given a dead enemy, generates an edible or otherwise useful body part from its corpse. ]",
  "placeholders": {
    "difficulty": "difficulty"
  },
  "aiParameters": {
    "temperature": 1.5,
    "repetition_penalty": 1.02
  },
  "properties": [
    {
      "name": "name",
      "replaceBy": "Dead Enemy:",
      "input": true
    },
    {
      "name": "type",
      "replaceBy": "Extracted Body Part Type:"
    },
    {
      "name": "rarity",
      "replaceBy": "Extracted Body Part Rarity:"
    },
    {
      "name": "item",
      "replaceBy": "Extracted Body Part Name:"
    }
  ],
  "list": [
    {
      "name": "Flaming Bear (challenging bear)",
      "item": "Self-Roasted Bear Meat",
      "type": "meat",
      "rarity": "rare"
    },
    {
      "name": "Orc Soldier (medium orc)",
      "item": "Ragged Orc Skin",
      "type": "hide",
      "rarity": "common"
    },
    {
      "name": "Rabid Unicorn (challenging equine)",
      "item": "Magical Unicorn Horn",
      "type": "component",
      "rarity": "uncommon"
    },
    {
      "name": "Rare Golden Dragon (arduous dragon )",
      "item": "Exquisite Golden Dragon Meat",
      "type": "meat",
      "rarity": "rare"
    },
    {
      "name": "Enraged Rabbit (easy rabbit)",
      "item": "Soft Rabbit Pelt",
      "type": "hide",
      "rarity": "very common"
    },
    {
      "name": "Feral Deer (easy deer)",
      "item": "Fine Deer Meat",
      "type": "meat",
      "rarity": "uncommon"
    },
    {
      "name": "Skeleton Knight (corpse) (challenging undead)",
      "item": "Solid Bone",
      "type": "component",
      "rarity": "uncommon"
    },
    {
      "name": "Dark Wizard (Very difficult wizard)",
      "item": "Tough Magic Wizard Meat",
      "type": "magic food",
      "rarity": "rare"
    },
    {
      "name": "Dire Wolf (corpse) (challenging canine)",
      "item": "Thick Wolf Pelt",
      "type": "hide",
      "rarity": "rare"
    },
    {
      "name": "Molten Lizard (medium reptile)",
      "item": "Smelly Fire-Resistant Lizard Meat",
      "type": "meat",
      "rarity": "uncommon"
    },
    {
      "name": "Undead Archmage (arduous unread)",
      "item": "Rotting Magic Infused Meat",
      "type": "meat",
      "rarity": "epic"
    },
    {
      "name": "Chameleon Ogre (challenging ogre)",
      "item": "Tasty Color-Shifting Ogre Meat",
      "type": "meat",
      "rarity": "rare"
    },
    {
      "name": "Arch-demon Overlord (impossible demon)",
      "item": "Glowing Evil Hellish Meat",
      "type": "magic meat",
      "rarity": "mythic"
    },
    {
      "name": "Flaming Tiger (challenging feline)",
      "item": "Charred Tiger Bones",
      "type": "component",
      "rarity": "uncommon"
    },
    {
      "name": "Orc archer (medium archer)",
      "item": "Keen Orc Eye",
      "type": "component",
      "rarity": "uncommon"
    },
    {
      "name": "Rabid Unicorn (difficult magical animal)",
      "item": "Infected Unicorn Meat",
      "type": "food",
      "rarity": "uncommon"
    },
    {
      "name": "Rare Golden Dragon (arduous dragon)",
      "item": "Golden Dragon Scales",
      "type": "component",
      "rarity": "epic"
    },
    {
      "name": "Enraged Rabbit (easy animal)",
      "item": "Rabbit foot of Enraged Luck",
      "type": "accessory",
      "rarity": "rare"
    },
    {
      "name": "Feral Deer (easy animal)",
      "item": "Deer Antler",
      "type": "component",
      "rarity": "common"
    },
    {
      "name": "Giant Ant Soldier (difficult insect)",
      "item": "Strong Acid",
      "type": "component",
      "rarity": "uncommon"
    },
    {
      "name": "Skeleton Knight (difficult undead)",
      "item": "Ancient Human Scull",
      "type": "component",
      "rarity": "rare"
    },
    {
      "name": "Dark Wizard (very challenging human)",
      "item": "Evil Magical Heart",
      "type": "component",
      "rarity": "epic"
    },
    {
      "name": "Young Treant (medium plant)",
      "item": "Treant Club",
      "type": "weapon",
      "rarity": "rare"
    },
    {
      "name": "Ice Lizard (medium reptile)",
      "item": "Frosty Lizard Meat",
      "type": "meat",
      "rarity": "uncommon"
    },
    {
      "name": "Wild Turkey (easy bird)",
      "item": "Succulent Turkey Breast Meat",
      "type": "meat",
      "rarity": "uncommon"
    },
    {
      "name": "Escaped Cow (easy bovine)",
      "item": "Cow Hide",
      "type": "hide",
      "rarity": "common"
    },
    {
      "name": "Cute White Kitten (trivial feline)",
      "item": "Innocent's Blood",
      "type": "component",
      "rarity": "common"
    }
  ]
}